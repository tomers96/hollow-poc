package producer;

import com.netflix.hollow.api.consumer.fs.HollowFilesystemAnnouncementWatcher;
import com.netflix.hollow.api.consumer.fs.HollowFilesystemBlobRetriever;
import com.netflix.hollow.api.producer.HollowProducer;
import com.netflix.hollow.api.producer.fs.HollowFilesystemAnnouncer;
import com.netflix.hollow.api.producer.fs.HollowFilesystemPublisher;
import com.opencsv.CSVReader;
import models.Branch;
import models.Relations;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Producer {
    public static final Path PUBLISH_PATH = Paths.get("D:\\Projects\\hollow-poc\\src\\publish-dir");
    private static final String DATA = "src/main/resources/data.csv";

    public static void main(String args[]) throws IOException {
        HollowFilesystemPublisher publisher = new HollowFilesystemPublisher(PUBLISH_PATH);
        HollowFilesystemAnnouncer announcer = new HollowFilesystemAnnouncer(PUBLISH_PATH);
        HollowFilesystemBlobRetriever blobRetriever =
                new HollowFilesystemBlobRetriever(PUBLISH_PATH);

        HollowFilesystemAnnouncementWatcher announcementWatcher =
                new HollowFilesystemAnnouncementWatcher(PUBLISH_PATH);


        HollowProducer producer = HollowProducer
                .withPublisher(publisher)
                .withAnnouncer(announcer)
                .build();

        producer.initializeDataModel(Relations.class, Branch.class);
        long latestAnnouncedVersion = announcementWatcher.getLatestVersion();
        producer.restore(latestAnnouncedVersion, blobRetriever);

        producer.runCycle(state -> {
            for (Relations relation : retrieveRelations())
                state.add(relation);
        });

    }

    private static List<Relations> retrieveRelations() throws IOException {
        Reader reader = Files.newBufferedReader(Paths.get(DATA));
        CSVReader csvReader = new CSVReader(reader);
        List<Relations> allRelations = new ArrayList<>();
        List<String[]> records = csvReader.readAll();
        for (String[] currRecord : records) {
            List<Branch> related = new ArrayList<>();
            Branch root = new Branch(currRecord[0], Integer.parseInt(currRecord[1]));
            for (int i = 2; i < currRecord.length - 1; i += 2) {
                related.add(new Branch(currRecord[i], Integer.parseInt(currRecord[i + 1])));
            }

            allRelations.add(new Relations(root, related));
        }

        return allRelations;
    }
}
