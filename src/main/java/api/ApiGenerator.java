package api;

import com.netflix.hollow.api.codegen.HollowAPIGenerator;
import com.netflix.hollow.core.write.HollowWriteStateEngine;
import com.netflix.hollow.core.write.objectmapper.HollowObjectMapper;
import models.Relations;

import java.io.IOException;

public class ApiGenerator {
    public static void main(String args[]) throws IOException {
        HollowWriteStateEngine writeEngine = new HollowWriteStateEngine();
        HollowObjectMapper mapper = new HollowObjectMapper(writeEngine);
        mapper.initializeTypeState(Relations.class);

        HollowAPIGenerator generator =
                new HollowAPIGenerator.Builder().withAPIClassname("RelationsAPI")
                        .withDestination("src\\main\\java")
                        .withPackageName("api.RelationsAPIPackage")
                        .withDataModel(writeEngine)
                        .build();

        generator.generateSourceFiles();
    }
}
