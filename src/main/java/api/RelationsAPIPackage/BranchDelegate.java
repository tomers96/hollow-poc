package api.RelationsAPIPackage;

import com.netflix.hollow.api.objects.delegate.HollowObjectDelegate;


@SuppressWarnings("all")
public interface BranchDelegate extends HollowObjectDelegate {

    public int getNameOrdinal(int ordinal);

    public int getType(int ordinal);

    public Integer getTypeBoxed(int ordinal);

    public BranchTypeAPI getTypeAPI();

}