package api.RelationsAPIPackage;

import com.netflix.hollow.api.consumer.HollowConsumer;
import com.netflix.hollow.api.consumer.index.AbstractHollowUniqueKeyIndex;
import com.netflix.hollow.api.consumer.index.HollowUniqueKeyIndex;
import com.netflix.hollow.core.schema.HollowObjectSchema;

/**
 * @deprecated see {@link com.netflix.hollow.api.consumer.index.UniqueKeyIndex} which can be built as follows:
 * <pre>{@code
 *     UniqueKeyIndex<Relations, K> uki = UniqueKeyIndex.from(consumer, Relations.class)
 *         .usingBean(k);
 *     Relations m = uki.findMatch(k);
 * }</pre>
 * where {@code K} is a class declaring key field paths members, annotated with
 * {@link com.netflix.hollow.api.consumer.index.FieldPath}, and {@code k} is an instance of
 * {@code K} that is the key to find the unique {@code Relations} object.
 */
@Deprecated
@SuppressWarnings("all")
public class RelationsPrimaryKeyIndex extends AbstractHollowUniqueKeyIndex<RelationsAPI, Relations> implements HollowUniqueKeyIndex<Relations> {

    public RelationsPrimaryKeyIndex(HollowConsumer consumer) {
        this(consumer, true);
    }

    public RelationsPrimaryKeyIndex(HollowConsumer consumer, boolean isListenToDataRefresh) {
        this(consumer, isListenToDataRefresh, ((HollowObjectSchema)consumer.getStateEngine().getNonNullSchema("Relations")).getPrimaryKey().getFieldPaths());
    }

    public RelationsPrimaryKeyIndex(HollowConsumer consumer, String... fieldPaths) {
        this(consumer, true, fieldPaths);
    }

    public RelationsPrimaryKeyIndex(HollowConsumer consumer, boolean isListenToDataRefresh, String... fieldPaths) {
        super(consumer, "Relations", isListenToDataRefresh, fieldPaths);
    }

    @Override
    public Relations findMatch(Object... keys) {
        int ordinal = idx.getMatchingOrdinal(keys);
        if(ordinal == -1)
            return null;
        return api.getRelations(ordinal);
    }

}