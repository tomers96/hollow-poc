package api.RelationsAPIPackage;

import com.netflix.hollow.api.objects.HollowList;
import com.netflix.hollow.core.schema.HollowListSchema;
import com.netflix.hollow.api.objects.delegate.HollowListDelegate;
import com.netflix.hollow.api.objects.generic.GenericHollowRecordHelper;

@SuppressWarnings("all")
public class ListOfBranch extends HollowList<Branch> {

    public ListOfBranch(HollowListDelegate delegate, int ordinal) {
        super(delegate, ordinal);
    }

    @Override
    public Branch instantiateElement(int ordinal) {
        return (Branch) api().getBranch(ordinal);
    }

    @Override
    public boolean equalsElement(int elementOrdinal, Object testObject) {
        return GenericHollowRecordHelper.equalObject(getSchema().getElementType(), elementOrdinal, testObject);
    }

    public RelationsAPI api() {
        return typeApi().getAPI();
    }

    public ListOfBranchTypeAPI typeApi() {
        return (ListOfBranchTypeAPI) delegate.getTypeAPI();
    }

}