package api.RelationsAPIPackage;

import com.netflix.hollow.api.custom.HollowObjectTypeAPI;
import com.netflix.hollow.core.read.dataaccess.HollowObjectTypeDataAccess;

@SuppressWarnings("all")
public class BranchTypeAPI extends HollowObjectTypeAPI {

    private final BranchDelegateLookupImpl delegateLookupImpl;

    public BranchTypeAPI(RelationsAPI api, HollowObjectTypeDataAccess typeDataAccess) {
        super(api, typeDataAccess, new String[] {
            "name",
            "type"
        });
        this.delegateLookupImpl = new BranchDelegateLookupImpl(this);
    }

    public int getNameOrdinal(int ordinal) {
        if(fieldIndex[0] == -1)
            return missingDataHandler().handleReferencedOrdinal("Branch", ordinal, "name");
        return getTypeDataAccess().readOrdinal(ordinal, fieldIndex[0]);
    }

    public StringTypeAPI getNameTypeAPI() {
        return getAPI().getStringTypeAPI();
    }

    public int getType(int ordinal) {
        if(fieldIndex[1] == -1)
            return missingDataHandler().handleInt("Branch", ordinal, "type");
        return getTypeDataAccess().readInt(ordinal, fieldIndex[1]);
    }

    public Integer getTypeBoxed(int ordinal) {
        int i;
        if(fieldIndex[1] == -1) {
            i = missingDataHandler().handleInt("Branch", ordinal, "type");
        } else {
            boxedFieldAccessSampler.recordFieldAccess(fieldIndex[1]);
            i = getTypeDataAccess().readInt(ordinal, fieldIndex[1]);
        }
        if(i == Integer.MIN_VALUE)
            return null;
        return Integer.valueOf(i);
    }



    public BranchDelegateLookupImpl getDelegateLookupImpl() {
        return delegateLookupImpl;
    }

    @Override
    public RelationsAPI getAPI() {
        return (RelationsAPI) api;
    }

}