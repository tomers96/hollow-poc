package api.RelationsAPIPackage;

import com.netflix.hollow.api.consumer.HollowConsumer;
import com.netflix.hollow.api.consumer.data.AbstractHollowDataAccessor;
import com.netflix.hollow.core.index.key.PrimaryKey;
import com.netflix.hollow.core.read.engine.HollowReadStateEngine;

@SuppressWarnings("all")
public class BranchDataAccessor extends AbstractHollowDataAccessor<Branch> {

    public static final String TYPE = "Branch";
    private RelationsAPI api;

    public BranchDataAccessor(HollowConsumer consumer) {
        super(consumer, TYPE);
        this.api = (RelationsAPI)consumer.getAPI();
    }

    public BranchDataAccessor(HollowReadStateEngine rStateEngine, RelationsAPI api) {
        super(rStateEngine, TYPE);
        this.api = api;
    }

    public BranchDataAccessor(HollowReadStateEngine rStateEngine, RelationsAPI api, String ... fieldPaths) {
        super(rStateEngine, TYPE, fieldPaths);
        this.api = api;
    }

    public BranchDataAccessor(HollowReadStateEngine rStateEngine, RelationsAPI api, PrimaryKey primaryKey) {
        super(rStateEngine, TYPE, primaryKey);
        this.api = api;
    }

    @Override public Branch getRecord(int ordinal){
        return api.getBranch(ordinal);
    }

}