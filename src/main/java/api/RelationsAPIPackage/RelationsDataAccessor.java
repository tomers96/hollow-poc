package api.RelationsAPIPackage;

import com.netflix.hollow.api.consumer.HollowConsumer;
import com.netflix.hollow.api.consumer.data.AbstractHollowDataAccessor;
import com.netflix.hollow.core.index.key.PrimaryKey;
import com.netflix.hollow.core.read.engine.HollowReadStateEngine;

@SuppressWarnings("all")
public class RelationsDataAccessor extends AbstractHollowDataAccessor<Relations> {

    public static final String TYPE = "Relations";
    private RelationsAPI api;

    public RelationsDataAccessor(HollowConsumer consumer) {
        super(consumer, TYPE);
        this.api = (RelationsAPI)consumer.getAPI();
    }

    public RelationsDataAccessor(HollowReadStateEngine rStateEngine, RelationsAPI api) {
        super(rStateEngine, TYPE);
        this.api = api;
    }

    public RelationsDataAccessor(HollowReadStateEngine rStateEngine, RelationsAPI api, String ... fieldPaths) {
        super(rStateEngine, TYPE, fieldPaths);
        this.api = api;
    }

    public RelationsDataAccessor(HollowReadStateEngine rStateEngine, RelationsAPI api, PrimaryKey primaryKey) {
        super(rStateEngine, TYPE, primaryKey);
        this.api = api;
    }

    @Override public Relations getRecord(int ordinal){
        return api.getRelations(ordinal);
    }

}