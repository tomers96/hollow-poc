package api.RelationsAPIPackage;

import com.netflix.hollow.api.consumer.HollowConsumer;
import com.netflix.hollow.api.consumer.index.UniqueKeyIndex;
import com.netflix.hollow.api.objects.HollowObject;
import com.netflix.hollow.core.schema.HollowObjectSchema;

@SuppressWarnings("all")
public class Branch extends HollowObject {

    public Branch(BranchDelegate delegate, int ordinal) {
        super(delegate, ordinal);
    }

    public HString getName() {
        int refOrdinal = delegate().getNameOrdinal(ordinal);
        if(refOrdinal == -1)
            return null;
        return  api().getHString(refOrdinal);
    }

    public int getType() {
        return delegate().getType(ordinal);
    }

    public Integer getTypeBoxed() {
        return delegate().getTypeBoxed(ordinal);
    }

    public RelationsAPI api() {
        return typeApi().getAPI();
    }

    public BranchTypeAPI typeApi() {
        return delegate().getTypeAPI();
    }

    protected BranchDelegate delegate() {
        return (BranchDelegate)delegate;
    }

    /**
     * Creates a unique key index for {@code Branch} that has a primary key.
     * The primary key is represented by the class {@link String}.
     * <p>
     * By default the unique key index will not track updates to the {@code consumer} and thus
     * any changes will not be reflected in matched results.  To track updates the index must be
     * {@link HollowConsumer#addRefreshListener(HollowConsumer.RefreshListener) registered}
     * with the {@code consumer}
     *
     * @param consumer the consumer
     * @return the unique key index
     */
    public static UniqueKeyIndex<Branch, String> uniqueIndex(HollowConsumer consumer) {
        return UniqueKeyIndex.from(consumer, Branch.class)
            .bindToPrimaryKey()
            .usingPath("name", String.class);
    }

}