package api.RelationsAPIPackage;

import java.util.Objects;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.Map;
import com.netflix.hollow.api.consumer.HollowConsumerAPI;
import com.netflix.hollow.api.custom.HollowAPI;
import com.netflix.hollow.core.read.dataaccess.HollowDataAccess;
import com.netflix.hollow.core.read.dataaccess.HollowTypeDataAccess;
import com.netflix.hollow.core.read.dataaccess.HollowObjectTypeDataAccess;
import com.netflix.hollow.core.read.dataaccess.HollowListTypeDataAccess;
import com.netflix.hollow.core.read.dataaccess.HollowSetTypeDataAccess;
import com.netflix.hollow.core.read.dataaccess.HollowMapTypeDataAccess;
import com.netflix.hollow.core.read.dataaccess.missing.HollowObjectMissingDataAccess;
import com.netflix.hollow.core.read.dataaccess.missing.HollowListMissingDataAccess;
import com.netflix.hollow.core.read.dataaccess.missing.HollowSetMissingDataAccess;
import com.netflix.hollow.core.read.dataaccess.missing.HollowMapMissingDataAccess;
import com.netflix.hollow.api.objects.provider.HollowFactory;
import com.netflix.hollow.api.objects.provider.HollowObjectProvider;
import com.netflix.hollow.api.objects.provider.HollowObjectCacheProvider;
import com.netflix.hollow.api.objects.provider.HollowObjectFactoryProvider;
import com.netflix.hollow.api.sampling.HollowObjectCreationSampler;
import com.netflix.hollow.api.sampling.HollowSamplingDirector;
import com.netflix.hollow.api.sampling.SampleResult;
import com.netflix.hollow.core.util.AllHollowRecordCollection;

@SuppressWarnings("all")
public class RelationsAPI extends HollowAPI  {

    private final HollowObjectCreationSampler objectCreationSampler;

    private final StringTypeAPI stringTypeAPI;
    private final BranchTypeAPI branchTypeAPI;
    private final ListOfBranchTypeAPI listOfBranchTypeAPI;
    private final RelationsTypeAPI relationsTypeAPI;

    private final HollowObjectProvider stringProvider;
    private final HollowObjectProvider branchProvider;
    private final HollowObjectProvider listOfBranchProvider;
    private final HollowObjectProvider relationsProvider;

    public RelationsAPI(HollowDataAccess dataAccess) {
        this(dataAccess, Collections.<String>emptySet());
    }

    public RelationsAPI(HollowDataAccess dataAccess, Set<String> cachedTypes) {
        this(dataAccess, cachedTypes, Collections.<String, HollowFactory<?>>emptyMap());
    }

    public RelationsAPI(HollowDataAccess dataAccess, Set<String> cachedTypes, Map<String, HollowFactory<?>> factoryOverrides) {
        this(dataAccess, cachedTypes, factoryOverrides, null);
    }

    public RelationsAPI(HollowDataAccess dataAccess, Set<String> cachedTypes, Map<String, HollowFactory<?>> factoryOverrides, RelationsAPI previousCycleAPI) {
        super(dataAccess);
        HollowTypeDataAccess typeDataAccess;
        HollowFactory factory;

        objectCreationSampler = new HollowObjectCreationSampler("String","Branch","ListOfBranch","Relations");

        typeDataAccess = dataAccess.getTypeDataAccess("String");
        if(typeDataAccess != null) {
            stringTypeAPI = new StringTypeAPI(this, (HollowObjectTypeDataAccess)typeDataAccess);
        } else {
            stringTypeAPI = new StringTypeAPI(this, new HollowObjectMissingDataAccess(dataAccess, "String"));
        }
        addTypeAPI(stringTypeAPI);
        factory = factoryOverrides.get("String");
        if(factory == null)
            factory = new StringHollowFactory();
        if(cachedTypes.contains("String")) {
            HollowObjectCacheProvider previousCacheProvider = null;
            if(previousCycleAPI != null && (previousCycleAPI.stringProvider instanceof HollowObjectCacheProvider))
                previousCacheProvider = (HollowObjectCacheProvider) previousCycleAPI.stringProvider;
            stringProvider = new HollowObjectCacheProvider(typeDataAccess, stringTypeAPI, factory, previousCacheProvider);
        } else {
            stringProvider = new HollowObjectFactoryProvider(typeDataAccess, stringTypeAPI, factory);
        }

        typeDataAccess = dataAccess.getTypeDataAccess("Branch");
        if(typeDataAccess != null) {
            branchTypeAPI = new BranchTypeAPI(this, (HollowObjectTypeDataAccess)typeDataAccess);
        } else {
            branchTypeAPI = new BranchTypeAPI(this, new HollowObjectMissingDataAccess(dataAccess, "Branch"));
        }
        addTypeAPI(branchTypeAPI);
        factory = factoryOverrides.get("Branch");
        if(factory == null)
            factory = new BranchHollowFactory();
        if(cachedTypes.contains("Branch")) {
            HollowObjectCacheProvider previousCacheProvider = null;
            if(previousCycleAPI != null && (previousCycleAPI.branchProvider instanceof HollowObjectCacheProvider))
                previousCacheProvider = (HollowObjectCacheProvider) previousCycleAPI.branchProvider;
            branchProvider = new HollowObjectCacheProvider(typeDataAccess, branchTypeAPI, factory, previousCacheProvider);
        } else {
            branchProvider = new HollowObjectFactoryProvider(typeDataAccess, branchTypeAPI, factory);
        }

        typeDataAccess = dataAccess.getTypeDataAccess("ListOfBranch");
        if(typeDataAccess != null) {
            listOfBranchTypeAPI = new ListOfBranchTypeAPI(this, (HollowListTypeDataAccess)typeDataAccess);
        } else {
            listOfBranchTypeAPI = new ListOfBranchTypeAPI(this, new HollowListMissingDataAccess(dataAccess, "ListOfBranch"));
        }
        addTypeAPI(listOfBranchTypeAPI);
        factory = factoryOverrides.get("ListOfBranch");
        if(factory == null)
            factory = new ListOfBranchHollowFactory();
        if(cachedTypes.contains("ListOfBranch")) {
            HollowObjectCacheProvider previousCacheProvider = null;
            if(previousCycleAPI != null && (previousCycleAPI.listOfBranchProvider instanceof HollowObjectCacheProvider))
                previousCacheProvider = (HollowObjectCacheProvider) previousCycleAPI.listOfBranchProvider;
            listOfBranchProvider = new HollowObjectCacheProvider(typeDataAccess, listOfBranchTypeAPI, factory, previousCacheProvider);
        } else {
            listOfBranchProvider = new HollowObjectFactoryProvider(typeDataAccess, listOfBranchTypeAPI, factory);
        }

        typeDataAccess = dataAccess.getTypeDataAccess("Relations");
        if(typeDataAccess != null) {
            relationsTypeAPI = new RelationsTypeAPI(this, (HollowObjectTypeDataAccess)typeDataAccess);
        } else {
            relationsTypeAPI = new RelationsTypeAPI(this, new HollowObjectMissingDataAccess(dataAccess, "Relations"));
        }
        addTypeAPI(relationsTypeAPI);
        factory = factoryOverrides.get("Relations");
        if(factory == null)
            factory = new RelationsHollowFactory();
        if(cachedTypes.contains("Relations")) {
            HollowObjectCacheProvider previousCacheProvider = null;
            if(previousCycleAPI != null && (previousCycleAPI.relationsProvider instanceof HollowObjectCacheProvider))
                previousCacheProvider = (HollowObjectCacheProvider) previousCycleAPI.relationsProvider;
            relationsProvider = new HollowObjectCacheProvider(typeDataAccess, relationsTypeAPI, factory, previousCacheProvider);
        } else {
            relationsProvider = new HollowObjectFactoryProvider(typeDataAccess, relationsTypeAPI, factory);
        }

    }

    public void detachCaches() {
        if(stringProvider instanceof HollowObjectCacheProvider)
            ((HollowObjectCacheProvider)stringProvider).detach();
        if(branchProvider instanceof HollowObjectCacheProvider)
            ((HollowObjectCacheProvider)branchProvider).detach();
        if(listOfBranchProvider instanceof HollowObjectCacheProvider)
            ((HollowObjectCacheProvider)listOfBranchProvider).detach();
        if(relationsProvider instanceof HollowObjectCacheProvider)
            ((HollowObjectCacheProvider)relationsProvider).detach();
    }

    public StringTypeAPI getStringTypeAPI() {
        return stringTypeAPI;
    }
    public BranchTypeAPI getBranchTypeAPI() {
        return branchTypeAPI;
    }
    public ListOfBranchTypeAPI getListOfBranchTypeAPI() {
        return listOfBranchTypeAPI;
    }
    public RelationsTypeAPI getRelationsTypeAPI() {
        return relationsTypeAPI;
    }
    public Collection<HString> getAllHString() {
        HollowTypeDataAccess tda = Objects.requireNonNull(getDataAccess().getTypeDataAccess("String"), "type not loaded or does not exist in dataset; type=String");
        return new AllHollowRecordCollection<HString>(tda.getTypeState()) {
            protected HString getForOrdinal(int ordinal) {
                return getHString(ordinal);
            }
        };
    }
    public HString getHString(int ordinal) {
        objectCreationSampler.recordCreation(0);
        return (HString)stringProvider.getHollowObject(ordinal);
    }
    public Collection<Branch> getAllBranch() {
        HollowTypeDataAccess tda = Objects.requireNonNull(getDataAccess().getTypeDataAccess("Branch"), "type not loaded or does not exist in dataset; type=Branch");
        return new AllHollowRecordCollection<Branch>(tda.getTypeState()) {
            protected Branch getForOrdinal(int ordinal) {
                return getBranch(ordinal);
            }
        };
    }
    public Branch getBranch(int ordinal) {
        objectCreationSampler.recordCreation(1);
        return (Branch)branchProvider.getHollowObject(ordinal);
    }
    public Collection<ListOfBranch> getAllListOfBranch() {
        HollowTypeDataAccess tda = Objects.requireNonNull(getDataAccess().getTypeDataAccess("ListOfBranch"), "type not loaded or does not exist in dataset; type=ListOfBranch");
        return new AllHollowRecordCollection<ListOfBranch>(tda.getTypeState()) {
            protected ListOfBranch getForOrdinal(int ordinal) {
                return getListOfBranch(ordinal);
            }
        };
    }
    public ListOfBranch getListOfBranch(int ordinal) {
        objectCreationSampler.recordCreation(2);
        return (ListOfBranch)listOfBranchProvider.getHollowObject(ordinal);
    }
    public Collection<Relations> getAllRelations() {
        HollowTypeDataAccess tda = Objects.requireNonNull(getDataAccess().getTypeDataAccess("Relations"), "type not loaded or does not exist in dataset; type=Relations");
        return new AllHollowRecordCollection<Relations>(tda.getTypeState()) {
            protected Relations getForOrdinal(int ordinal) {
                return getRelations(ordinal);
            }
        };
    }
    public Relations getRelations(int ordinal) {
        objectCreationSampler.recordCreation(3);
        return (Relations)relationsProvider.getHollowObject(ordinal);
    }
    public void setSamplingDirector(HollowSamplingDirector director) {
        super.setSamplingDirector(director);
        objectCreationSampler.setSamplingDirector(director);
    }

    public Collection<SampleResult> getObjectCreationSamplingResults() {
        return objectCreationSampler.getSampleResults();
    }

}
