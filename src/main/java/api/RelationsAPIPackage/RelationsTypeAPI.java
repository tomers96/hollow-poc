package api.RelationsAPIPackage;

import com.netflix.hollow.api.custom.HollowObjectTypeAPI;
import com.netflix.hollow.core.read.dataaccess.HollowObjectTypeDataAccess;

@SuppressWarnings("all")
public class RelationsTypeAPI extends HollowObjectTypeAPI {

    private final RelationsDelegateLookupImpl delegateLookupImpl;

    public RelationsTypeAPI(RelationsAPI api, HollowObjectTypeDataAccess typeDataAccess) {
        super(api, typeDataAccess, new String[] {
            "root",
            "branches"
        });
        this.delegateLookupImpl = new RelationsDelegateLookupImpl(this);
    }

    public int getRootOrdinal(int ordinal) {
        if(fieldIndex[0] == -1)
            return missingDataHandler().handleReferencedOrdinal("Relations", ordinal, "root");
        return getTypeDataAccess().readOrdinal(ordinal, fieldIndex[0]);
    }

    public BranchTypeAPI getRootTypeAPI() {
        return getAPI().getBranchTypeAPI();
    }

    public int getBranchesOrdinal(int ordinal) {
        if(fieldIndex[1] == -1)
            return missingDataHandler().handleReferencedOrdinal("Relations", ordinal, "branches");
        return getTypeDataAccess().readOrdinal(ordinal, fieldIndex[1]);
    }

    public ListOfBranchTypeAPI getBranchesTypeAPI() {
        return getAPI().getListOfBranchTypeAPI();
    }

    public RelationsDelegateLookupImpl getDelegateLookupImpl() {
        return delegateLookupImpl;
    }

    @Override
    public RelationsAPI getAPI() {
        return (RelationsAPI) api;
    }

}