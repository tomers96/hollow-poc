package api.RelationsAPIPackage;

import com.netflix.hollow.api.objects.delegate.HollowObjectAbstractDelegate;
import com.netflix.hollow.core.read.dataaccess.HollowObjectTypeDataAccess;
import com.netflix.hollow.core.schema.HollowObjectSchema;
import com.netflix.hollow.api.custom.HollowTypeAPI;
import com.netflix.hollow.api.objects.delegate.HollowCachedDelegate;

@SuppressWarnings("all")
public class RelationsDelegateCachedImpl extends HollowObjectAbstractDelegate implements HollowCachedDelegate, RelationsDelegate {

    private final int rootOrdinal;
    private final int branchesOrdinal;
    private RelationsTypeAPI typeAPI;

    public RelationsDelegateCachedImpl(RelationsTypeAPI typeAPI, int ordinal) {
        this.rootOrdinal = typeAPI.getRootOrdinal(ordinal);
        this.branchesOrdinal = typeAPI.getBranchesOrdinal(ordinal);
        this.typeAPI = typeAPI;
    }

    public int getRootOrdinal(int ordinal) {
        return rootOrdinal;
    }

    public int getBranchesOrdinal(int ordinal) {
        return branchesOrdinal;
    }

    @Override
    public HollowObjectSchema getSchema() {
        return typeAPI.getTypeDataAccess().getSchema();
    }

    @Override
    public HollowObjectTypeDataAccess getTypeDataAccess() {
        return typeAPI.getTypeDataAccess();
    }

    public RelationsTypeAPI getTypeAPI() {
        return typeAPI;
    }

    public void updateTypeAPI(HollowTypeAPI typeAPI) {
        this.typeAPI = (RelationsTypeAPI) typeAPI;
    }

}