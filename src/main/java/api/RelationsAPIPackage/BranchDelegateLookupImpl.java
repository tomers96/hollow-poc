package api.RelationsAPIPackage;

import com.netflix.hollow.api.objects.delegate.HollowObjectAbstractDelegate;
import com.netflix.hollow.core.read.dataaccess.HollowObjectTypeDataAccess;
import com.netflix.hollow.core.schema.HollowObjectSchema;

@SuppressWarnings("all")
public class BranchDelegateLookupImpl extends HollowObjectAbstractDelegate implements BranchDelegate {

    private final BranchTypeAPI typeAPI;

    public BranchDelegateLookupImpl(BranchTypeAPI typeAPI) {
        this.typeAPI = typeAPI;
    }

    public int getNameOrdinal(int ordinal) {
        return typeAPI.getNameOrdinal(ordinal);
    }

    public int getType(int ordinal) {
        return typeAPI.getType(ordinal);
    }

    public Integer getTypeBoxed(int ordinal) {
        return typeAPI.getTypeBoxed(ordinal);
    }

    public BranchTypeAPI getTypeAPI() {
        return typeAPI;
    }

    @Override
    public HollowObjectSchema getSchema() {
        return typeAPI.getTypeDataAccess().getSchema();
    }

    @Override
    public HollowObjectTypeDataAccess getTypeDataAccess() {
        return typeAPI.getTypeDataAccess();
    }

}