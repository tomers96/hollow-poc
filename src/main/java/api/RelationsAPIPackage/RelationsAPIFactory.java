package api.RelationsAPIPackage;

import com.netflix.hollow.api.client.HollowAPIFactory;
import com.netflix.hollow.api.custom.HollowAPI;
import com.netflix.hollow.api.objects.provider.HollowFactory;
import com.netflix.hollow.core.read.dataaccess.HollowDataAccess;
import java.util.Collections;
import java.util.Set;

@SuppressWarnings("all")
public class RelationsAPIFactory implements HollowAPIFactory {

    private final Set<String> cachedTypes;

    public RelationsAPIFactory() {
        this(Collections.<String>emptySet());
    }

    public RelationsAPIFactory(Set<String> cachedTypes) {
        this.cachedTypes = cachedTypes;
    }

    @Override
    public HollowAPI createAPI(HollowDataAccess dataAccess) {
        return new RelationsAPI(dataAccess, cachedTypes);
    }

    @Override
    public HollowAPI createAPI(HollowDataAccess dataAccess, HollowAPI previousCycleAPI) {
        if (!(previousCycleAPI instanceof RelationsAPI)) {
            throw new ClassCastException(previousCycleAPI.getClass() + " not instance of RelationsAPI");        }
        return new RelationsAPI(dataAccess, cachedTypes, Collections.<String, HollowFactory<?>>emptyMap(), (RelationsAPI) previousCycleAPI);
    }

}