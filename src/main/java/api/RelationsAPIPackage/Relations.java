package api.RelationsAPIPackage;

import com.netflix.hollow.api.consumer.HollowConsumer;
import com.netflix.hollow.api.objects.HollowObject;
import com.netflix.hollow.core.schema.HollowObjectSchema;

@SuppressWarnings("all")
public class Relations extends HollowObject {

    public Relations(RelationsDelegate delegate, int ordinal) {
        super(delegate, ordinal);
    }

    public Branch getRoot() {
        int refOrdinal = delegate().getRootOrdinal(ordinal);
        if(refOrdinal == -1)
            return null;
        return  api().getBranch(refOrdinal);
    }

    public ListOfBranch getBranches() {
        int refOrdinal = delegate().getBranchesOrdinal(ordinal);
        if(refOrdinal == -1)
            return null;
        return  api().getListOfBranch(refOrdinal);
    }

    public RelationsAPI api() {
        return typeApi().getAPI();
    }

    public RelationsTypeAPI typeApi() {
        return delegate().getTypeAPI();
    }

    protected RelationsDelegate delegate() {
        return (RelationsDelegate)delegate;
    }

}