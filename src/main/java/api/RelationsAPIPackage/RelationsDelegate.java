package api.RelationsAPIPackage;

import com.netflix.hollow.api.objects.delegate.HollowObjectDelegate;


@SuppressWarnings("all")
public interface RelationsDelegate extends HollowObjectDelegate {

    public int getRootOrdinal(int ordinal);

    public int getBranchesOrdinal(int ordinal);

    public RelationsTypeAPI getTypeAPI();

}