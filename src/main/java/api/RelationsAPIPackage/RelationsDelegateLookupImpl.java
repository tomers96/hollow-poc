package api.RelationsAPIPackage;

import com.netflix.hollow.api.objects.delegate.HollowObjectAbstractDelegate;
import com.netflix.hollow.core.read.dataaccess.HollowObjectTypeDataAccess;
import com.netflix.hollow.core.schema.HollowObjectSchema;

@SuppressWarnings("all")
public class RelationsDelegateLookupImpl extends HollowObjectAbstractDelegate implements RelationsDelegate {

    private final RelationsTypeAPI typeAPI;

    public RelationsDelegateLookupImpl(RelationsTypeAPI typeAPI) {
        this.typeAPI = typeAPI;
    }

    public int getRootOrdinal(int ordinal) {
        return typeAPI.getRootOrdinal(ordinal);
    }

    public int getBranchesOrdinal(int ordinal) {
        return typeAPI.getBranchesOrdinal(ordinal);
    }

    public RelationsTypeAPI getTypeAPI() {
        return typeAPI;
    }

    @Override
    public HollowObjectSchema getSchema() {
        return typeAPI.getTypeDataAccess().getSchema();
    }

    @Override
    public HollowObjectTypeDataAccess getTypeDataAccess() {
        return typeAPI.getTypeDataAccess();
    }

}