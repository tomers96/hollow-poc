package api.RelationsAPIPackage;

import com.netflix.hollow.api.objects.delegate.HollowObjectAbstractDelegate;
import com.netflix.hollow.core.read.dataaccess.HollowObjectTypeDataAccess;
import com.netflix.hollow.core.schema.HollowObjectSchema;
import com.netflix.hollow.api.custom.HollowTypeAPI;
import com.netflix.hollow.api.objects.delegate.HollowCachedDelegate;

@SuppressWarnings("all")
public class BranchDelegateCachedImpl extends HollowObjectAbstractDelegate implements HollowCachedDelegate, BranchDelegate {

    private final int nameOrdinal;
    private final Integer type;
    private BranchTypeAPI typeAPI;

    public BranchDelegateCachedImpl(BranchTypeAPI typeAPI, int ordinal) {
        this.nameOrdinal = typeAPI.getNameOrdinal(ordinal);
        this.type = typeAPI.getTypeBoxed(ordinal);
        this.typeAPI = typeAPI;
    }

    public int getNameOrdinal(int ordinal) {
        return nameOrdinal;
    }

    public int getType(int ordinal) {
        if(type == null)
            return Integer.MIN_VALUE;
        return type.intValue();
    }

    public Integer getTypeBoxed(int ordinal) {
        return type;
    }

    @Override
    public HollowObjectSchema getSchema() {
        return typeAPI.getTypeDataAccess().getSchema();
    }

    @Override
    public HollowObjectTypeDataAccess getTypeDataAccess() {
        return typeAPI.getTypeDataAccess();
    }

    public BranchTypeAPI getTypeAPI() {
        return typeAPI;
    }

    public void updateTypeAPI(HollowTypeAPI typeAPI) {
        this.typeAPI = (BranchTypeAPI) typeAPI;
    }

}