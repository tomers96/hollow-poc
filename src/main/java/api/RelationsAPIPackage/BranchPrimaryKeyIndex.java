package api.RelationsAPIPackage;

import com.netflix.hollow.api.consumer.HollowConsumer;
import com.netflix.hollow.api.consumer.index.AbstractHollowUniqueKeyIndex;
import com.netflix.hollow.api.consumer.index.HollowUniqueKeyIndex;
import com.netflix.hollow.core.schema.HollowObjectSchema;

/**
 * @deprecated see {@link com.netflix.hollow.api.consumer.index.UniqueKeyIndex} which can be built as follows:
 * <pre>{@code
 *     UniqueKeyIndex<Branch, K> uki = UniqueKeyIndex.from(consumer, Branch.class)
 *         .usingBean(k);
 *     Branch m = uki.findMatch(k);
 * }</pre>
 * where {@code K} is a class declaring key field paths members, annotated with
 * {@link com.netflix.hollow.api.consumer.index.FieldPath}, and {@code k} is an instance of
 * {@code K} that is the key to find the unique {@code Branch} object.
 */
@Deprecated
@SuppressWarnings("all")
public class BranchPrimaryKeyIndex extends AbstractHollowUniqueKeyIndex<RelationsAPI, Branch> implements HollowUniqueKeyIndex<Branch> {

    public BranchPrimaryKeyIndex(HollowConsumer consumer) {
        this(consumer, true);
    }

    public BranchPrimaryKeyIndex(HollowConsumer consumer, boolean isListenToDataRefresh) {
        this(consumer, isListenToDataRefresh, ((HollowObjectSchema)consumer.getStateEngine().getNonNullSchema("Branch")).getPrimaryKey().getFieldPaths());
    }

    public BranchPrimaryKeyIndex(HollowConsumer consumer, String... fieldPaths) {
        this(consumer, true, fieldPaths);
    }

    public BranchPrimaryKeyIndex(HollowConsumer consumer, boolean isListenToDataRefresh, String... fieldPaths) {
        super(consumer, "Branch", isListenToDataRefresh, fieldPaths);
    }

    @Override
    public Branch findMatch(Object... keys) {
        int ordinal = idx.getMatchingOrdinal(keys);
        if(ordinal == -1)
            return null;
        return api.getBranch(ordinal);
    }

}