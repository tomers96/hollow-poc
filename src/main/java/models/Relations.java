package models;

import java.util.List;

public class Relations {
    private Branch root;
    private List<Branch> branches;

    public Relations(Branch root, List<Branch> branches) {
        this.root = root;
        this.branches = branches;
    }
}
