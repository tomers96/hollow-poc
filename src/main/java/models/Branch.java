package models;

import com.netflix.hollow.core.write.objectmapper.HollowPrimaryKey;

@HollowPrimaryKey(fields="name")
public class Branch {
    public String name;
    public int type;

    public Branch(String name, Integer type) {
        this.name = name;
        this.type = type;
    }

}
