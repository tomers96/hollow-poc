package consumer;

import api.RelationsAPIPackage.Branch;
import api.RelationsAPIPackage.Relations;
import api.RelationsAPIPackage.RelationsAPI;
import com.netflix.hollow.api.consumer.HollowConsumer;
import com.netflix.hollow.api.consumer.fs.HollowFilesystemAnnouncementWatcher;
import com.netflix.hollow.api.consumer.fs.HollowFilesystemBlobRetriever;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Consumer {
    public static final Path PUBLISH_PATH = Paths.get("D:\\Projects\\hollow-poc\\src\\publish-dir");

    public static void main(String args[]) throws IOException, InterruptedException {

        HollowFilesystemBlobRetriever blobRetriever =
                new HollowFilesystemBlobRetriever(PUBLISH_PATH);

        HollowFilesystemAnnouncementWatcher announcementWatcher =
                new HollowFilesystemAnnouncementWatcher(PUBLISH_PATH);

        HollowConsumer consumer = HollowConsumer.newHollowConsumer()
                .withBlobRetriever(blobRetriever)
                .withAnnouncementWatcher(announcementWatcher)
                .withGeneratedAPIClass(RelationsAPI.class)
                .build();

        while (true) {
            consumer.triggerRefresh();
            RelationsAPI relationsAPI = (RelationsAPI) consumer.getAPI();

            for (Relations relations : relationsAPI.getAllRelations()) {
                System.out.println("Root: " + relations.getRoot().getName().getValue() + ", " +
                        relations.getRoot().getType());
                System.out.println("\n ========== BRANCHES ======== \n");
                for (Branch branch : relations.getBranches()) {
                    System.out.println(branch.getName().getValue() + ", " + branch.getType() + "\n");
                }
            }
            Thread.sleep(1000);

        }
    }
}
